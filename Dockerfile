FROM tensorflow/tensorflow:1.13.1-py3

ADD . /facenet_webservice/src
WORKDIR /facenet_webservice/src

RUN apt-get autoclean
RUN apt-get update
RUN apt-get install -y python3-pip python3-dev
RUN pip3 install --upgrade pip
RUN apt install -y cmake
RUN apt-get install -y libsm6 libxext6 libxrender-dev
RUN apt-get install -y python-zbar git nano libglib2.0-0
RUN pip3 install -r requirements.txt
COPY . /src

EXPOSE $5000
CMD python3 app.py
