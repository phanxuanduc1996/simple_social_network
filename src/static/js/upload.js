$(document).ready(function() {
    $('#submit_upload').prop('disabled', true);

    $("#content_upload").on("keyup", action);

    function action() {
        if (($('#content_upload').val().length > 0)) {
            $('#submit_upload').prop("disabled", false);
        } else {
            $('#submit_upload').prop("disabled", true);
        }
    }

    $('#submit_upload').click(function() {
        var confirm_register = confirm("Are you sure want to post ?");
        if (confirm_register) {
            var content_upload =
              document.getElementById("content_upload").value;

            $.ajax({
              url: "/upload/",
              data: {
                content_upload: content_upload,
              },
              success: function (data) {
                // alert("The article has been posted successfully.");
                console.log(data["content_upload"]);
              },
              error: function (error) {
                console.log("Register Error :" + error);
              },
            });
        } else
            return false;
    });
});