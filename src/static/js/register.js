$(document).ready(function () {
  $("#submit_register").prop("disabled", true);
  $("#user_name").on("keyup", action);
  $("#full_name").on("keyup", action);
  $("#email").on("keyup", action);
  $("#password").on("keyup", action);
  $("#re_password").on("keyup", action);

//   function validateEmail(email) {
//     const re =
//       /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     return re.test(email);
//   }

//   function validate() {
//     const $result = $("#error_email");
//     const email = $("#email").val();
//     $result.text("");

//     if (validateEmail(email)) {
//       $result.text(email + " is valid :)");
//       $result.css("color", "green");
//     } else {
//       $result.text(email + " is not valid :(");
//       $result.css("color", "red");
//     }
//     return false;
//   }

//   $("#email").on("input", validate);

  function action() {
    if (
      $("#user_name").val().length > 0 &&
      $("#full_name").val().length > 0 &&
      $("#email").val().length > 0 &&
      $("#password").val().length > 0 &&
      $("#re_password").val().length > 0
    ) {
      $("#submit_register").prop("disabled", false);
    } else {
      $("#submit_register").prop("disabled", true);
    }
  }

  // Submit for register users.
  $("#submit_register").click(function () {
    var user_name = document.getElementById("user_name").value;
    var full_name = document.getElementById("full_name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var re_password = document.getElementById("re_password").value;

    $.ajax({
      url: "/register",
      data: {
        user_name: user_name,
        full_name: full_name,
        email: email,
        password: password,
        re_password: re_password,
      },
      success: function (data) {
        console.log("Register successfully!");
      },
      error: function (error) {
        alert("That username already exists!");
        console.log("Register Error :" + error);
      },
    });
  });
});

function changeTypePassword() {
  var value_pw = document.getElementById("password");
  var value_re_pw = document.getElementById("re_password");
  if (value_pw.type === "password") {
    value_pw.type = "text";
    value_re_pw.type = "text";
  } else {
    value_pw.type = "password";
    value_re_pw.type = "password";
  }
}
