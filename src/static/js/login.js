$(document).ready(function() {
    $('#submit_login').prop("disabled", true);
    $('#user_name').on("keyup", action);
    $('#password').on("keyup", action);

    function action() {
        if (($('#user_name').val().length > 0) && ($('#password').val().length > 0)) {
            $('#submit_login').prop("disabled", false);
        } else {
            $('#submit_login').prop("disabled", true);
        }
    }

    // Submit for login users.
    $('#submit_login').click(function() {
        var full_name = document.getElementById("user_name").value;
        var password = document.getElementById("password").value;

        $.ajax({
            url: '/login',
            data: {
                user_name: full_name,
                password: password,
            },
            success: function(data) {
                console.log('Login successfully!');
            },
            error: function(error) {
                console.log('Login Error :' + error);
            }
        });
    });
});

function changeTypePassword() {
    var value = document.getElementById("password");
    if (value.type === "password") {
        value.type = "text";
    } else {
        value.type = "password";
    }
}