from flask import Flask, render_template, request, redirect, url_for, make_response, jsonify, session, json
from flask_pymongo import PyMongo
from flask_paginate import Pagination, get_page_args, get_page_parameter
from flask_assets import Environment, Bundle
from flask_fontawesome import FontAwesome
from functools import wraps
from werkzeug.utils import secure_filename
from flask_bcrypt import Bcrypt
import pymongo
import os
import ast
import sys
import argparse
import cv2
import base64
from OpenSSL import crypto
from flask_socketio import SocketIO
import numpy as np
import scipy.misc as misc
from PIL import Image
import io
import shutil
import datetime


app = Flask(__name__)
app.config['ENV'] = os.environ.get('FLASK_ENV')
app.config['SECRET_KEY'] = 'secret!'

PORT = 5037

assets = Environment(app)
assets.url = app.static_url_path
assets.debug = True
scss = Bundle('sass/custom.scss', filters='pyscss', output='gen/all.css')
assets.register('scss_all', scss)

FontAwesome(app)
socketio = SocketIO(app)

app.config["MONGO_URI"] = "mongodb://localhost:27017/databaseStudents"
mongo = PyMongo(app)
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["databaseStudents"]
users = mydb["students"]
posts = mydb["posts"]
bcrypt = Bcrypt(None)


def auth_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if auth and auth.username == 'admin' and auth.password == 'admin':
            return f(*args, **kwargs)
        return make_response("Could not verify your login !!!", 401,
                             {'WWW-Authenticate': 'Basic realm="Login Required"'})
    return decorated


navbar_children = 'SIMPLE SOCIAL NETWORK'


@app.route("/", methods=['POST', 'GET'])
@auth_required
def home():
    """ Home of the application """
    if 'user_name' not in session:
        return redirect(url_for('login'))

    if request.method == 'POST':
        if request.form['btn-upload'] == 'Upload':
            return redirect(url_for(upload))
        elif request.form['btn-list_users'] == 'List Users':
            return redirect(url_for(list_users))
        return render_template('main.html', title=navbar_children)
    else:
        return render_template('main.html', title=navbar_children)


@app.route("/upload/", methods=['POST', 'GET'])
@auth_required
def upload():
    """ Home of the "Register" section """
    if 'user_name' not in session:
        return redirect(url_for('login'))

    if request.method == "POST":
        data = {"success": False}
        # Get information of the registered customer
        content_upload = request.form["content_upload"]
        user_name = session['user_name']
        time_created = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")

        posts.insert(
            {'user_name': user_name, 'content': content_upload, 'time_created': time_created})

        data['success'] = True
        data['content_upload'] = content_upload

        return render_template("upload.html", title=navbar_children)

    if request.method == "GET":
        return render_template("upload.html", title=navbar_children)


def list_users(offset=0, per_page=10):
    return list_users[offset: offset + per_page]


# Show list user.
@app.route("/list_users/", methods=['POST', 'GET'])
@auth_required
def list_users():
    """ Show registered user list """
    if 'user_name' not in session:
        return redirect(url_for('login'))

    if request.method == "GET":
        database = posts.find().sort("time_created", -1)

        print(database)

        # Get the information of each user.
        list_posts = []
        for ele in database:
            dict_post = {}
            dict_post['content'] = ele["content"]
            dict_post['user_name'] = ele["user_name"]
            list_posts.append(dict_post)

        total = len(list_posts)

        # Pagination display, 10 users displayed in 1 page.
        page, per_page, offset = get_page_args(
            page_parameter='page', per_page_parameter='per_page')
        pagination_users = list_posts[offset: offset + per_page]
        pagination = Pagination(
            page=page, per_page=per_page, total=total, css_framework='bootstrap4')

        return render_template("list_users.html", list_users=pagination_users, page=page, per_page=per_page,
                               pagination=pagination, title=navbar_children)


@app.route("/login", methods=['POST', 'GET'])
@auth_required
def login():
    if 'user_name' in session:
        return redirect(url_for('home'))

    error_login = ""
    data = {"success": False}
    if request.method == 'POST':
        user_name = request.form.get('user_name')
        login_user = users.find_one({'user_name': user_name})
        print(login_user)
        if not login_user is None:
            if bcrypt.check_password_hash(login_user['password'], request.form.get('password')):
                session['user_name'] = user_name
                data['success'] = True
                print(data['success'])
                error_login = ""
                return redirect(url_for('home'))

        print(data['success'])
        data['success'] = False
        error_login = 'Invalid username or password. Please try again!'
        return render_template('login.html', title=navbar_children, error_login=error_login)

    if request.method == 'GET':
        error_login = ""
        return render_template('login.html', title=navbar_children, error_login=error_login)


@app.route('/register', methods=['POST', 'GET'])
@auth_required
def register():
    if 'user_name' in session:
        return redirect(url_for('home'))

    error_register = ""
    data = {"success": False}
    if request.method == 'POST':
        user_name = request.form.get('user_name')
        full_name = request.form.get('full_name')
        email = request.form.get('email')
        password = request.form.get('password')
        re_password = request.form.get('re_password')
        hashpass = bcrypt.generate_password_hash(password)

        if (len(user_name.split(" ")) > 1 or len(password.split(" ")) > 1):
            error_register = 'Blank exists in username or password!'
            return render_template('register.html', title=navbar_children, error_register=error_register)
        if (password != re_password):
            error_register = 'Please check the password again!'
            return render_template('register.html', title=navbar_children, error_register=error_register)
        else:
            existing_user = users.find_one(
                {'user_name': user_name})
            if not existing_user:
                users.insert(
                    {'user_name': user_name, 'full_name': full_name, 'email': email, 'password': hashpass})
                print(users)
                data = {"success": True}
                return redirect(url_for('login'))
            else:
                error_register = 'That username already exists!'
                return render_template('register.html', title=navbar_children, error_register=error_register)

    else:
        error_register = ""
        return render_template('register.html', title=navbar_children, error_register=error_register)


@app.route('/logout/')
@auth_required
def logout():
    session.pop('user_name', None)
    return redirect(url_for('login'))


# Create certificate for https
def create_self_signed_cert(certfile, keyfile, certargs, cert_dir="."):
    C_F = os.path.join(cert_dir, certfile)
    K_F = os.path.join(cert_dir, keyfile)
    if not os.path.exists(C_F) or not os.path.exists(K_F):
        k = crypto.PKey()
        k.generate_key(crypto.TYPE_RSA, 1024)
        cert = crypto.X509()
        cert.get_subject().C = certargs["Country"]
        cert.get_subject().ST = certargs["State"]
        cert.get_subject().L = certargs["City"]
        cert.get_subject().O = certargs["Organization"]
        cert.get_subject().OU = certargs["Org. Unit"]
        cert.get_subject().CN = 'Example'
        cert.set_serial_number(1000)
        cert.gmtime_adj_notBefore(0)
        cert.gmtime_adj_notAfter(315360000)
        cert.set_issuer(cert.get_subject())
        cert.set_pubkey(k)
        cert.sign(k, 'sha1')
        open(C_F, "wb").write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
        open(K_F, "wb").write(crypto.dump_privatekey(crypto.FILETYPE_PEM, k))


if __name__ == "__main__":
    CERT_FILE = "cert.pem"
    KEY_FILE = "key.pem"
    create_self_signed_cert(CERT_FILE, KEY_FILE, certargs={
        "Country": "US", "State": "NY", "City": "Ithaca", "Organization": "Python-Bugs",
        "Org. Unit": "Proof of Concept"})
    socketio.run(app, host="0.0.0.0", certfile=CERT_FILE,
                 keyfile=KEY_FILE, port=PORT, debug=True)
